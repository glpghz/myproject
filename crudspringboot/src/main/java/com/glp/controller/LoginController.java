package com.glp.controller;



import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

@Controller
public class LoginController {


    @PostMapping("/user/login")
    public String login(@RequestParam("username" ) String username,
                        @RequestParam("password") String password, Model model, HttpSession session){
        if (!StringUtils.isEmpty(username)&&"123456".equals(password)){
            session.setAttribute("user",username);
            System.out.println("表单重复提交");
            return "redirect:/main.html";
        }else{
            model.addAttribute("msg","用户名密码错误");
            return "login";
        }

    }
}
